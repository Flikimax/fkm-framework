<?php

class Home extends MainController {

	public $vista;
	public $modelos;
	public $nombreColumnas;

	public function __construct (){
		$modelosCargar = array('Usuario');
		$this->modelos = $this->modelos($modelosCargar);
		$this->nombreColumnas = $this->modelos['usuario']->nombreColumnas();
	}

	public function MetodoDefault($parametros){
		$usuarios = $this->modelos['usuario']->selects();


		$clientes = $this->modelos['usuario']->selects("clientes");
		$nombreColumnasClientes = $this->modelos['usuario']->nombreColumnas("clientes");

		$agregarTable = ["Editar", "Borrar"];


		$formulario = $this->crearTabla("table table-condensed", $nombreColumnasClientes, $clientes, $agregarTable);


		//$this->vista = $this->vista(__CLASS__ ,__CLASS__, $usuarios); 
		//echo $this->vista;
	}

	public function agregar(){
 			
			//----Nombres columnas tabla
		$arrayColumnas = $this->modelos['usuario']->nombreColumnas();

		//$this->form($arrayColumnas);

		//-----------------------------------------

 		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
 			$datos = [
 				'nombre' => trim($_POST['nombre']),
 				'email' => trim($_POST['email']),
 				'telefono' => trim($_POST['telefono']),
 			];
 			if ($this->modelos['usuario']->insertar("datos", $datos)){
 				Router::redireccionar(HOME.__CLASS__);
 			}else{
 				Router::redireccionar(HOME.ERROR_PAGE);
 			}
 		}else{
 			$datos = [
 				'nombre' => '',
 				'email' => '',
 				'telefono' => '',
 			];
 			$this->vista = $this->vista(__CLASS__, "agregar", $datos); 
 		}
 			
		echo $this->vista;

  	}

  		public function editar($id){

  			$datos = [];
  			if (REQUEST_METHOD == 'POST'){
  				$datos = [
					'id' => $id,
					'nombre' => trim($_POST['nombre']),
					'email' => trim($_POST['email']),
					'telefono' => trim($_POST['telefono']),
				];

				if ($this->modelos['usuario']->actualizarFila($datos, "datos")){
					Router::redireccionar(HOME.__CLASS__);
				}else{
					Router::redireccionar(HOME.ERROR_PAGE);
				}
  			}else {
  				//Se obtiene informacion de usuario desde el modelo
				$datos = $this->modelos['usuario']->fila($id);

				echo $this->vista(__CLASS__, "editar", $datos); 
  			}

  		}

  		public function borrar($id){
		   $datos = $this->modelos['usuario']->fila($id);

			if ($_SERVER['REQUEST_METHOD'] == 'POST'){
				if ($this->modelos['usuario']->borrarFila($datos, "datos")){
					Router::redireccionar(HOME.__CLASS__);
				}else{
					Router::redireccionar(HOME.ERROR_PAGE);
				}
			}

			$this->vista = $this->vista(__CLASS__, "borrar", $datos); 
			echo $this->vista;
		}

}