<?php

class Usuario extends MainModel {

	public function obtenerUsuarios(){
		return $this->selects();
	}

	public function obtenerUltimoID($table){
		return $this->ultimoId($table);
	}

	public function agregarUsuario($datos){
		return $this->insertar("datos", $datos);
	}

	public function obtenerUsuario($id){
		return $this->fila($id);
	}

	public function actualizarUsuario($datos){
		return $this->actualizarFila($datos, "datos");
	}

	public function borrarUsuario($datos){
		return $this->borrarFila($datos, "datos");
	}

}
