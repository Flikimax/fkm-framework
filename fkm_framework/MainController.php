<?php

	# ================================================
	# =      CLASE CONTROLADOR PRINCIPAL             =
	# = SE ENCARGA DE CARGA LOS MODELOS Y LAS VISTAS =
	# ================================================

	abstract class MainController {

		public $modeloCargar;

		# = METODO PARA REQUERIR VISTAS; RETORNA LA VISTA EN UNA VARIABLE =
		protected function modelos($modelos){

			if (is_array($modelos)){
				$arrayObjetos = [];
				
				foreach ($modelos as $key) {
					$modelo = strtolower($key);
					$objetoModelo  = new $key;
					$arrayObjetos[$modelo] = $objetoModelo;
				}
				return $arrayObjetos;

			}else {
				return new $modelos;

			}
		}

		# = METODO PARA REQUERIR VISTAS; RETORNA LA VISTA EN UNA VARIABLE =
		protected function vista($nombreControlador, $nombreVista = '', $parametros = array()){
    		
    		$rutaArchivo = RUTA_VISTAS . $nombreVista . '.php';

			if (is_file($rutaArchivo)){
				/*if (is_array($parametros)){
					echo "<pre>".LINEA;print_r($parametros); echo LINEA."</pre>";
					extract($parametros);
					echo LINEA; print_r($usuario1); echo LINEA;
				}*/
				
				ob_start();

				require($rutaArchivo);
				$temp = ob_get_contents();

				ob_end_clean();
				return $temp;

			}else{

				throw new Exception(LINEA."Error: No existe la vista-> $rutaArchivo".LINEA);
			}
  		}

  		

  		protected function crearTabla($claseTabla, $columnas, $datos, $agregarColumnas = null, $omitirColumnas = null){

  			$longitud = count($columnas);
  			
  			//----------------------------------
  			if ($agregarColumnas){
  				if (is_array($agregarColumnas)){
  					$columnas = $this->pushArrayInArray($columnas, $agregarColumnas);
  				}else{
  					array_push($columnas, $agregarColumnas);
  				}
  			}


			require RUTA_INC . 'header.php'; ?>

				<table class="table table-condensed">
					<thead>
						<tr>
							<?php 
								foreach ($columnas as $value) {
									echo "<th>".$value."</th>";
								}

							 ?>
						</tr>
					</thead>

					<tbody>

						<?php

						$lon = count($columnas);

						echo "<pre>";print_r($columnas[0]);echo "</pre>";
						echo LINEA."<pre>";print_r($datos[0]);echo "</pre>".LINEA;

						/*for ($i=0, $longitud = count($datos); $i < $longitud; $i++) { 
							for ($i=0; $i < ; $i++) { 
								# code...
							}
						}*/


						?>


						<?php foreach($datos as $usuario): ?>
							<tr>
								<?php 
									for ($i=0; $i < $longitud; $i++) { 
										echo "<td>".$usuario[$columnas[$i]]."</td>";
									}
								 ?>

								<td><a href="<?=HOME?>home/editar/<?=$usuario['id']?>">Editar</a></td>
				
								<td><a href="<?=HOME?>home/borrar/<?=$usuario['id']?>">Borrar</a></td>

							</tr>
						<?php endforeach; ?>

					</tbody>
				</table>

			<?php require RUTA_INC . 'footer.php'; ?>







			<?php 


  			return false;
  		}

  		public static function pushArrayInArray($arrayBase, $array){
  			for ($i=0, $long = count($array); $i < $long; $i++) { 
				array_push($arrayBase, $array[$i]);  				
  			}
  			return $arrayBase;

  		}

  		# = METODO REQUERIDO PARA TODOS LOS CONTROLADORES HIJOS =
  		abstract public function MetodoDefault($parametros);

	}
