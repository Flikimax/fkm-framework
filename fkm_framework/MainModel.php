<?php

	# =============================================
	# =       CLASE PRINCIPAL DE DATA BASE        =
	# =============================================

	class MainModel {

		public $tabla;
  		public $objectoDB;

		public function __construct($tablaC = TABLE_DEFAULT, $dataBaseC = DATABASE_DEFAULT){

			//Conexion
			$objectoDB = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, $dataBaseC);
			if ($objectoDB->connect_errno) {
			    echo "<br>Fallo al conectar a MySQL: " . $mysqli->connect_error;
			}
			$this->objectoDB = $objectoDB;
		    //VALIDAR TABLA EN LA BASE DE DATOS
		    $this->tabla = $tablaC;
		    $this->comprobarTablaDB($this->tabla);
		}


		# =============================================
		# =        Cheque si la tabla exite           =
		# =============================================

		function comprobarTablaDB ($tabla){

		    if ($tabla){
		    	$results = $this->objectoDB->query("SHOW TABLES LIKE '$tabla'");
			    if ($results->num_rows > 0) {
			      return $tabla;
			    }else {
			    	//Error, la tabla no se encuentra
			        die("<br><hr/>Error: La tabla no existe en la base de datos<hr/><br>");
			    }


		    }else {
			    //Error, cadena vacia
			    echo (LINEA."Error: El parámetro recibido esta vacio.".LINEA);
		    }

		}







		///----------------------------Re organizar----------------------------

		public function procesarQuery($query){
			if ($query) {
				$resultado = mysqli_query($this->objectoDB, $query);
				if (!$resultado) {
				    echo "Fallo al ejecutar la consulta: (" . $mysqli->errno . ") " . $mysqli->error."<br> CONSULTA: $query";
				}
				else {
					return $resultado;
				}
			}
			else {
				return "Consulta vacia";
			}
		}

		//-----------------------------------------------------------------------------------
		//RETORNA ARRAY DE ARRAY
		public function selects ($selectsTabla = TABLE_DEFAULT, $columnas="*", $condiciones=null, $limites=null, $ordenes=null){
		    $results = $this->procesarQuery("SELECT $columnas FROM $selectsTabla");
		    
		    $results->fetch_assoc();
		    $arrayDB = null;
		    foreach ($results as $filaKey => $result){
		      foreach ($result as $columnaKey => $columna) {
		        $arrayDB[$filaKey][$columnaKey] = $columna;
		      }
		    }

		    return $arrayDB;
		 }

		 public function fila ($id, $columnas="*", $condiciones=null, $limites=null, $ordenes=null){
		    $results = $this->procesarQuery("SELECT $columnas FROM $this->tabla WHERE id='$id'");

		    $results->fetch_assoc();
		    $arrayDB = null;
		    foreach ($results as $filaKey => $result){
		      foreach ($result as $columnaKey => $columna) {
		        $arrayDB[$filaKey][$columnaKey] = $columna;
		      }
		    }

		    return $arrayDB[0];
		 }

		public function actualizarFila ($datos, $table){
			$rs = $this->procesarQuery("UPDATE $table SET nombre='$datos[nombre]', email='$datos[email]', telefono='$datos[telefono]' WHERE id='$datos[id]'");
			return $rs;
		}

		public function borrarFila ($datos, $table){
			$rs = $this->procesarQuery("DELETE FROM $table WHERE id='$datos[id]'");

			if ($rs === true) {
			    return true;
			} else {
			    return false;
			}
		}

		//-----------------------------------------------------------------------------

		public function insertar($table, $datos){

			$rs = $this->procesarQuery("INSERT INTO $table (nombre, email, telefono) VALUES ('".$datos['nombre']."', '".$datos['email']."', '".$datos['telefono']."')");
			return $rs;

		}

		 //RETORNA EL ID DEL ULTIMO REGISTROS (MAX(id))
		function ultimoId ($table){
		    $rs = $this->objectoDB->query("SELECT MAX(id) FROM $table");
		    
		    if ($row = $rs->fetch_row()) {
		      $id = trim($row[0]);
		    }

		    return $id;
		}





















		# =============================================
		# =     Cheque si la Base de Datos exite      =
		# =============================================

		function checkDb($objectDB, $dataBase){
		    if($objectoDB->connect_errno) {
		       die("<br><br>Error al conectarse a MySQL: (" . $objectoDB->connect_errno . ")");
		    }else {
		      	$result = $objectoDB->select_db($dataBase);
		      	if (!$result) {
			        if ($die){
			        	//Error al conectarse a la base de datos $dataBase
			        	die("<br><br>Error initializing object");
			        }else{
			        	$objectoDB->select_db(DATABASE_DEFAULT);
			        }
		   		}
		    	return $objectoDB;

			}
		}

		function nombreColumnas ($tabla = TABLE_DEFAULT){
			$rs = $this->procesarQuery("SHOW COLUMNS FROM $tabla");
			$columnas = [];			

			foreach ($rs as $key) {
				array_push($columnas, $key['Field']);
				//echo LINEA. '<pre> CLASS MODEL:'; print_r($key); echo '</pre>' .LINEA;
			}
			return $columnas;

		}


	}
























