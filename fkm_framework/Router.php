<?php

	class Router {

		public $uri;
		public $controlador;
		public $metodo;
		public $parametro;
 
		function __construct(){
			$this->setUri();
			$this->setControlador();
			$this->setMetodo();
			$this->setParametro();
		}

		/**
	    * Asigna la uri completa
	    */
		public function setUri(){
      		$this->uri = explode('/', URI);
		}

		/**
	    *Asigna el nombre del controlador
	    */
		public function setControlador(){
			$this->controlador = $this->uri[NUM_CONTROLADOR] === '' ? DEFAULT_CONTROLADOR : $this->uri[NUM_CONTROLADOR];
		}

		public function setMetodo(){
			$this->metodo = !empty($this->uri[NUM_CONTROLADOR+1]) ? $this->uri[NUM_CONTROLADOR+1] : DEFAULT_METODO;
		}

		public function setParametro(){
			$this->parametro = !empty($this->uri[NUM_CONTROLADOR+2]) ? $this->uri[NUM_CONTROLADOR+2] : '';
		}

		public function getUri(){
			return $this->uri;
		}

		public function getControlador(){
			return $this->controlador;
		}

		public function getMetodo(){
			return $this->metodo;
		}

		public function getParametro(){
			return $this->parametro;
		}

		//Para redireccionar la pagina
		static public function redireccionar ($url = HOME){
			header('location: ' . $url);

		}

	}
