<?php 
	# =============================================
	# =   		       AUTOLOAD                   =
	# =============================================

	spl_autoload_register(function ($className) {
		//$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
		
		# =   CLASES PRINCIPALES FRAMEWORK  =
		if (is_file(RUTA_FRAMEWORK . "$className.php")){
			return require RUTA_FRAMEWORK . "$className.php";	
		}# =   CLASES MODELOS   =
	    else if (is_file(RUTA_MODELOS . "$className.php")){
	    	return include_once RUTA_MODELOS . $className .'.php';
	    }# =   CLASES DE AYUDA FRAMEWORK  =
		else if(is_file(RUTA_HELPS . "$className.php")){
		    return require RUTA_HELPS . "$className.php";
		}

		

	});

	//http://php.net/manual/es/language.namespaces.importing.php
		


