<?php

	# ===========================================================================================
	# =     CONFIGURAR LAS PRIMERAS 7 CONSTANTES Y LOS DATOS REQUERIDOS POR LA BASE DE DATOS    =
	# ===========================================================================================

	define ('NAME_SITE', 'FLIKIMAX MVC');

	define ('NOMBRE_CARPETA_FRAMEWORK', 'fkm_framework');
	define ('NOMBRE_CARPETA_APP', 'app');

	# =     DENTRO DE LA CARPETA APP 		  =
	define ('NOMBRE_CARPETA_MODELOS', 'models');
	define ('NOMBRE_CARPETA_VISTAS', 'views');
	define ('NOMBRE_CARPETA_CONTROLADORES', 'controllers');

	# =     	   CARPETA DE RECURSOS DE LA VISTA (inc)	        =
	# =      			   DENTRO DE LA CARPETA   		            =
	# = NOMBRE_CARPETA_APP/NOMBRE_CARPETA_VISTAS/NOMBRE_CARPETA_INC =
	define ('NOMBRE_CARPETA_INC', 'inc');

	# =============================================
	# =                DEFAULT 	            	  =
	# =============================================

	define ('DEFAULT_CONTROLADOR', 'Home');
	define ('DEFAULT_METODO', 'MetodoDefault');
	define ('ERROR_PAGE', 'ErrorPage');

	# ====================================================
	# ======  FIN DE LAS CONFIGURACIONES MANUALES  =======
	# ====================================================

	# =============================================================================== #
	# ==========================CONFIGURACIONES AUTOMATICAS========================== #
	# =============================================================================== #

	# =============================================
	# =			  RUTAS DEl PROYECTO     		  =
	# =============================================

	define ('ROOT', $_SERVER['DOCUMENT_ROOT']);
	
	//$arrayRoot = explode("/", ROOT);
	define ('RUTA_PROYECTO', dirname(dirname(dirname(__FILE__))));

	define ('RUTA_FRAMEWORK', RUTA_PROYECTO.'/'.NOMBRE_CARPETA_FRAMEWORK.'/');
	define ('RUTA_APP', RUTA_PROYECTO .'/'. NOMBRE_CARPETA_APP);

	define ('RUTA_MODELOS', RUTA_APP.'/'.NOMBRE_CARPETA_MODELOS.'/');
	define ('RUTA_VISTAS', RUTA_APP.'/'.NOMBRE_CARPETA_VISTAS.'/');
	define ('RUTA_CONTROLADORES', RUTA_APP.'/'.NOMBRE_CARPETA_CONTROLADORES.'/');

	define ('RUTA_HELPS', RUTA_FRAMEWORK . 'helps/');
	define ('RUTA_HTML', RUTA_FRAMEWORK . 'html/');

	define ('RUTA_INC', RUTA_VISTAS . NOMBRE_CARPETA_INC.'/');

	# =			  RUTAS URL 		  =
	define('URI', $_SERVER["REQUEST_URI"]);
	define('HOST', $_SERVER["HTTP_HOST"]);

	RouterHelper::host_vhost();

	define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);

	# =============================================
	# = 		   ECHO REUTILIZABLES 			  =
	# =============================================

	define ('LINEA', '<hr/>');

	# ===============================================
	# =  FUNCION PARA LA CREACION DE ALGUNAS RUTAS  =
	# =     AYUDA A LA UBICACION DEL CONTROLADOR    =
	# ===============================================

