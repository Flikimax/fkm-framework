<?php 

class RouterHelper {
	
	public static function validarControlador($controlador) {
    if(!is_file(RUTA_CONTROLADORES . ucwords(strtolower($controlador)).'.php')){
      return false;

    }
    return true;

  }

  public static function validarMetodoControlador($controlador, $metodo) {
    if(!method_exists($controlador, $metodo)){
      Router::redireccionar(HOME.$controlador);
      
      return false;
    }
    return true;

  }

  public static function host_vhost(){
    $arrayRutaProyecto = explode("/", RUTA_PROYECTO);

    $ultimo = (end($arrayRutaProyecto));
    $arrayUri = explode("/", URI);

    for ($i=0, $longitud = count($arrayUri); $i < $longitud; $i++) { 
      $carpetaProyecto[$i] = $arrayUri[$i];
      if ($arrayUri[$i] == $ultimo){
        break;
      }  
    }
    $home = (implode("/", $carpetaProyecto))."/";
          
    define('HOME', $home);
    define('URL_HOME', HOST.$home);
      
    # = POSICION EN LA QUE EL CONTROLADOR COMIENZA =
    define ('NUM_CONTROLADOR', count($carpetaProyecto));

  }

}