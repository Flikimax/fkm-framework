<?php require RUTA_INC . 'header.php'; ?>

	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefono</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>

			<?php foreach($parametros as $usuario): ?>
				<tr>
					<td> <?php echo $usuario['id'] ?> </td>
					<td> <?php echo $usuario['nombre'] ?> </td>
					<td> <?php echo $usuario['email'] ?> </td>
					<td> <?php echo $usuario['telefono'] ?> </td>
					<td><a href="<?=HOME?>home/editar/<?=$usuario['id']?>">Editar</a></td>
	
					<td><a href="<?=HOME?>home/borrar/<?=$usuario['id']?>">Borrar</a></td>
				</tr>
			<?php endforeach; ?>

		</tbody>
	</table>

<?php require RUTA_INC . 'footer.php'; ?>