
<?php
	
	# =============================================
	# =    SE CARGAN LOS ARHIVOS DEL FRAMEWORK	  =
	# =============================================
	require 'autoload.php';
	require 'config/config.php';
	require RUTA_APP . '/config.php';

	# ===============================================
	# =       SE INSTANCIA LA CLASE router          =
	# =		 MANEJO DE URLS - CARPETA RAIZ		  	=
	# = 0-Empty									  	=
	# = 1-fkm-framewor							  	=
	# = 2-Controladores							  	=
	# = 3-Metodo								  	=
	# = 4-Parametros							  	=
	# = Ejemplo:									=
	# = Fkm-framewor/controlador/metodo/parametros  =
	# ===============================================

	# =  VALORES DE RUTA  =

	$router = new Router;

	$controlador = $router->getControlador();
	$metodo = $router->getMetodo();
	$parametros = $router->getParametro();

	# =  VALIDACIONES E INCLUSION DEL CONTROLADOR Y EL METODO =

	if(!RouterHelper::validarControlador($controlador)){
		$controlador = ERROR_PAGE;
	}else {
		$controlador = ucwords(strtolower($controlador));
		$metodo = strtolower($metodo);
	}
  	
	require RUTA_CONTROLADORES . $controlador . '.php';

	if(!RouterHelper::validarMetodoControlador($controlador, $metodo)){
  		$metodo = DEFAULT_METODO;
	}

	# = EJECUCION FINAL DEL CONTROLADOR, METODO Y PARAMETROS OBTENIDOS POR URI =

	$controlador = new $controlador;
	$controlador->$metodo($parametros);
